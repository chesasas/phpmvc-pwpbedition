<?php

require_once('../app/core/Controller.php');

class Login extends Controller {

    public function __construct() {
        if($_SESSION["id"]) {
            header("Location: home");
        }
    }

    public function index() {
        $data["title"] = "Login";
        $this->view('auth/login');
    }

    public function storeLogin() {
        if($this->model('UserModel')->findUserBy($_POST["username"], $_POST["username"])) {
            $loggedUser = $this->model('UserModel')->login($_POST);

            if($loggedUser) {
                $this->model('UserModel')->createSession($loggedUser);
                Flasher::setFlash("Selamat Anda Berhasil", " Login", "success");
            } else {
                $data["title"] = "Login";
                $data["msg"] = "Kata Sandi Salah!";
                $this->view('auth/login', $data);
            }
        } else {
            $data["title"] = "Login";
            $data["msg"] = "Username tidak di temukan.";
            $this->view('auth/login', $data);
        }
    }

    public function store() {
        if($this->model("UserModel")->createUser($_POST)) {
            header('Location:'. BASE_URL .'home');
            Flasher::setFlash("Selamat, Anda Berhasil Menambahkan", " Data", "success");
            exit;
        }
    }

    public function logout() {
        $_SESSION = array();
        header('Location: '. BASE_URL .'home');
    }
}

?>