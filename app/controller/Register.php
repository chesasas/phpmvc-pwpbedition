<?php

require_once('../app/core/Controller.php');

class Register extends Controller {

    public function index() {
        $data['title'] = 'Register';
        $this->view('auth/register');
    }

    public function store() {
        if($this->model("UserModel")->createUser($_POST)) {
            header('Location:'. BASE_URL .'login/index');
            Flasher::setFlash("Selamat, Anda Berhasil", " Registrasi", "success");
            exit;
        }
    }
}

?>