<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="" id="">
        <?php Flasher::flash(); ?>
    </div>
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <?php
            if(empty($_SESSION["id"])) {
                echo "
                    <h1 class='h2'>Selamat Datang</h1>
                    ";
            } else {
                echo "
                <h1 class='h2'>Selamat Datang, ". $_SESSION['username'] ."</h1>
                    ";
            }
        ?>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
            </div>
            <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <span data-feather="calendar" class="align-text-bottom"></span>
                This week
            </button>
        </div>
    </div>

    <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>

    <div class="d-flex justify-content-between" id="">
        <h2>Data User</h2>
            <?php
                if($_SESSION["id"]) {
                    echo '
                        <button type="button" class="btn btn-sm btn-outline-secondary" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@mdo">Tambah Data</button>
                        ';
                }
            ?>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data User</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="<?= BASE_URL ?>login/store" method="POST">
                    <div class="modal-body">
                        <div class="mb-1">
                            <label for="message-text" class="col-form-label">Username</label>
                            <input type="text" class="form-control" name="username" id="username">
                        </div>
                        <div class="mb-1">
                            <label for="recipient-name" class="col-form-label">Email</label>
                            <input type="text" class="form-control" name="email" id="email">
                        </div>
                        <div class="mb-1">
                            <label for="message-text" class="col-form-label">Nama Depan</label>
                            <input type="text" class="form-control" name="first_name" id="first_name">
                        </div>
                        <div class="mb-1">
                            <label for="message-text" class="col-form-label">Nama Belakang</label>
                            <input type="text" class="form-control" name="last_name" id="last_name">
                        </div>
                        <div class="mb-1">
                            <label for="message-text" class="col-form-label">Sandi</label>
                            <input type="text" class="form-control" name="pass" id="pass">
                        </div>
                        <div class="mb-1">
                            <label for="message-text" class="col-form-label">Konfirmasi Sandi</label>
                            <input type="text" class="form-control" name="confirmpass" id="confirmpass">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <input type="submit" class="btn btn-primary" value="Buat" name="save" id="save">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-sm">
        <thead>
            <tr class="text-center">
                <th scope="col">No.</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col">Nama Depan</th>
                <th scope="col">Nama Belakang</th>
            </tr>
        </thead>

        
        <tbody>
            <?php foreach($data['name'] as $rows) : ?>
            <tr class="text-center">
                <td><?= $rows['id'] ?></td>
                <td><?= $rows['username'] ?></td>
                <td><?= $rows['email'] ?></td>
                <td><?= $rows['first_name'] ?></td>
                <td><?= $rows['last_name'] ?></td>
            </tr>
            <?php endforeach ?>
        </tbody>
        </table>
    </div>
</main>