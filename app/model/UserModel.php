<?php

class UserModel {
    private $table = 'users',
            $db,
            $name = 'Chesa';
    public function getUser() {
        return $this->name;
    }
    public function __construct() {
        $this->db = new Database();
    }
    public function getAllUser() {
        $this->db->query("SELECT * FROM {$this->table}");
        return $this->db->resultAll();
    }
    public function getUserByID($id) {
        $this->db->query("SELECT * FROM {$this->table} WHERE id=:id");
        $this->db->bind('id', $id);
        return $this->db->resultSingle();
    }
    public function createUser($data) {
        $pass = password_hash($data["pass"], PASSWORD_BCRYPT);

        $this->db->query("INSERT INTO phpmvc.users(username, email, first_name, last_name, pass, confirmpass) VALUES (:username, :email, :first_name, :last_name, :pass, :confirmpass)");

        $this->db->bind(':username', $data['username']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':first_name', $data['first_name']);
        $this->db->bind(':last_name', $data['last_name']);
        $this->db->bind(':pass', $pass);
        $this->db->bind(':confirmpass', $data['confirmpass']);

        $this->db->execute();

        return $this->db->rowCount();
    }

    public function findUserBy($username, $email) {
        $this->db->query("SELECT * FROM {$this->table} WHERE `username` = :username OR `email` = :email");
        $this->db->bind('username', $username);
        $this->db->bind('email', $email);
        $row  = $this->db->resultSingle();

        if($this->db->rowCount() > 0) {
            return $row;
        } else {
            return false;
        }
    }

    public function login($data) {
        $userMail = htmlspecialchars($data["username"]);
        $row = $this->findUserBy($userMail, $userMail);
        
        if($row == false) return false;
        $hashedPass = $row["pass"];

        if(password_verify($data["pass"], $hashedPass)) {
            return $row;
        } else {
            return false;
        }
    }

    public function createSession($user) {
        $_SESSION["id"] = $user["id"];
        $_SESSION["username"] = $user["username"];
        $_SESSION["email"] = $user["email"];
        $_SESSION["first_name"] = $user["first_name"];
        $_SESSION["last_name"] = $user["last_name"];
        $_SESSION["token"] = $user["token"];

        header('Location: '. BASE_URL .'home/');
    }
}